const pizza = {
    pizzaSize: {
        size:'',
        price: null
    },
    pizzaSauce: {
        sauce: '',
        size:'',
        price: null
    },
    pizzaToping: {
        toping: '',
        size:'',
        price: null
    },
    get price() {
        return pizza.pizzaSize.price + this.pizzaSauce.price + pizza.pizzaToping.price;
    },
    get sauce() {
        return pizza.pizzaSauce.sauce;
    },
    get toping(){
        return pizza.pizzaToping.toping;
    }
};

document.querySelector("#pizza").addEventListener("click", (e) => {});

const [...lebAr] = document.querySelectorAll("label > input");
lebAr.forEach((item) => {
    item.addEventListener("click", (e) => {
        console.dir(item);
        if (item.checked) {
            const input = item;
            if (input.id == "mid") {
                pizza.pizzaSize = {
                    size: "mid",
                    price: 50,
                };
            } else if (input.id == "small") {
                pizza.pizzaSize = {
                    size: "small",
                    price: 30,
                };
            } else if (input.id == "big") {
                pizza.pizzaSize = {
                    size: "big",
                    price: 80,
                };
            }
            showInfo();
        }
    });
});

function showInfo() {
    document.querySelector(".price > p").innerHTML = "Ціна: " + pizza.price;
    document.querySelector('.sauces > p').innerHTML = 'Соуси:' + pizza.sauce;
    document.querySelector('.topings > p').innerHTML = 'Топiнги:' + pizza.toping;
}

document.querySelector('.ingridients').addEventListener('click', (e) => {});
const [...sauceAr] = document.querySelectorAll('.sauce > img');
const [...topingAr] = document.querySelectorAll('.toping > img');
sauceAr.forEach((item) => {
    item.addEventListener('dragenter', (e) => {
        if (item.click){
            const input = item;
            if (input.id == "sauceClassic"){
                pizza.pizzaSauce = {
                    sauce:'Кетчуп',
                    price: 10,
                };
            } else if (input.id == 'sauceBBQ'){
                pizza.pizzaSauce = {
                    sauce:'BBQ',
                    price: 10,
                };
            } else if (input.id == 'sauceRikotta'){
                pizza.pizzaSauce = {
                    sauce:'Рiкотта',
                    price: 10,
                };
            } else if (input.id == undefined) {
                pizza.pizzaSauce = {
                    sauce: ' ',
                };
            }
            showInfo();
        }
    });

});
topingAr.forEach((item) => {
    item.addEventListener('dragenter', (e) => {
        if (item.click){
            const input = item;
            if (input.id == 'moc1'){
                pizza.pizzaToping = {
                    toping:'Сир звичайний',
                    price: 15,
                };
            } else if (input.id == 'moc2'){
                pizza.pizzaToping = {
                    toping:'Сир фета',
                    price: 15,
                };
            } else if (input.id == 'moc3'){
                pizza.pizzaToping = {
                    toping:'Моцарелла',
                    price: 15,
                };
            } else if (input.id == 'telya'){
                pizza.pizzaToping = {
                    toping:'Телятина',
                    price: 15,
                };
            } else if (input.id == 'vetch1'){
                pizza.pizzaToping = {
                    toping:'Помiдори',
                    price: 15,
                };
            } else if (input.id == 'vetch2'){
                pizza.pizzaToping = {
                    toping:'Гриби',
                    price: 15,
                };
            } else if (input.id == undefined){
                pizza.pizzaToping = {
                    toping: ' ',
                };
            }
            showInfo();
        }
    });

});

const dragEl = document.querySelectorAll('.draggable');
const dropZones = document.querySelectorAll('.table');

dragEl.forEach(e => {
    e.draggable = "true";
})

for (let i = 0, length = dragEl.length; i < length; i++) {
    dragEl[i].setAttribute('data-item', `${i}`);
}

dragEl.forEach(dragItem => {
    dragItem.addEventListener('dragstart', dragstart);
});

dropZones.forEach(dropZone => {
    dropZone.addEventListener('dragenter', dragenter);
    dropZone.addEventListener('dragover', dragover);
    dropZone.addEventListener('drop', drop);
})

function dragstart(e) {
    e.dataTransfer.setData('dragEl', this.dataset.item)
};

function dragenter(e) {
    e.preventDefault();
};

function dragover(e) {
    e.preventDefault();
};
function drop(e) {
    const dragFlag = e.dataTransfer.getData('dragEl');
    const dragItem = document.querySelector(`[data-item="${dragFlag}"]`)
    const clone = dragItem.cloneNode(true)


    this.append(clone);
};

function validate( el ){
    function validateEl( reg, val, name ){

        reg.test(val);

        if( val.search(reg) !== -1 ){
            document.querySelector(`.error-${name}`).style.display = 'none';
        }else{
            document.querySelector(`.error-${name}`).style.display = 'block';
        }

        cancel.addEventListener('click', deleteError)
        function deleteError() {
            document.querySelector(`.error-${name}`).style.display = 'none';
        }
    }

    switch( el.id ){
        case 'name':            
            validateEl( /^[А-я]+$/ , el.value, 'name'); 
        break;
        case 'phone':
            validateEl( /^\+38\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/ , el.value, 'phone');
        break;
        case 'email':
            validateEl( /^[A-z1-9._]+@[a-z1-9._]+.[a-z]{1,4}$/ , el.value, 'email');
        break;
    }
}

const [...inputArr] = document.querySelectorAll('#contactInfo input');

inputArr.filter( function(el){
    return el.id === 'name' || el.id === 'phone' || el.id === 'email';
}).forEach( function(el){

    const   inputId = document.getElementById(el.id),
            errorName = `Пожалуйста, введите правельное имя!`,
            errorTel = `Пожалуйста, введите номер в формате +38(XXX)XXX-XX-XX`,
            errorEmail = `Пожалуйста, введите корректный адрес электронной почты!`,
            offSetTop = inputId.offsetTop - 40,
            errorDiv = document.createElement('div');

    if (el.id === 'name') {
        errorDiv.innerText = errorName;
    } else if (el.id === 'phone') {
        errorDiv.innerText = errorTel;
    } else if (el.id === 'email') {
        errorDiv.innerText = errorEmail;
    }
    document.querySelector('#contactInfo').append(errorDiv);
    errorDiv.style.cssText = `
        top: ${offSetTop}px;
        left: ${inputId.offsetLeft}px;
    `
    errorDiv.classList.add(`error-${el.id}`, 'error');

    el.addEventListener('change', function(){
        validate(this);
    });
});

banner.addEventListener('mouseover', scam, false)
function scam() {
    this.style.left = getRandom() + '%';
    this.style.top = getRandom() + '%';
}

function getRandom() {
    const random = Math.floor(Math.random() * 80 + 1);
    return random;
}




